from __future__ import absolute_import, unicode_literals

from flask import g
from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy_utils import PhoneNumberType, ArrowType

from src.auth.constants import UserRoleEnum
from src.core.db.mixins import SoftDeletesMixin, TrackedModificationsMixin
from src.extensions import db


def user_id_from_context(context):
    if context.current_parameters.get('modified_by'):
        return context.current_parameters['modified_by']
    return context.current_parameters['user_id']


class User(UserMixin, SoftDeletesMixin, TrackedModificationsMixin, db.Model):
    __tablename__ = 'users'

    name = db.Column(db.Text, nullable=False)
    type = db.Column(db.Enum(UserRoleEnum, native_enum=False, create_constraint=False),
                     server_default=UserRoleEnum.GENERIC,
                     nullable=False)
    phone_number = db.Column(PhoneNumberType(region='IN'), nullable=False)

    # retuns a query object instead of a collection. This query can be further wrangled.
    query__logins = db.dynamic_loader(lambda: Token, backref='user')
    # query__user_auth_details = db.dynamic_loader(lambda: UserAuthDetails, backref='user')  # TODO twilio/plivo


# TODO twilio/plivo
# class UserAuthDetails(SoftDeletesMixin, TrackedModificationsMixin, db.Model):
#     __tablename__ = 'user_auth_details'
#
#     user_id = db.Column(db.Integer, db.ForeignKey(User.__table__.c.id), nullable=False)
#     twilio_user_id = db.Column(db.Text, nullable=False)  # Remove null constraint when more columns later


class Token(TrackedModificationsMixin, db.Model):
    __tablename__ = 'token'

    user_id = db.Column(db.Integer, db.ForeignKey(User.__table__.c.id), nullable=False)
    access_token = db.Column(UUID, nullable=False, unique=True)
    expires_at = db.Column(ArrowType, nullable=False)

    created_by = db.Column(db.Integer, default=user_id_from_context, nullable=False)
    modified_by = db.Column(db.Integer, default=user_id_from_context,
                            onupdate=lambda context: g.current_user.id,
                            nullable=False)
