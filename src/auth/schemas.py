from __future__ import absolute_import, unicode_literals

from src.auth.models.sqla import User, Token
from src.core.serializers import ArrowField
from src.extensions import ma


class TokenSchema(ma.ModelSchema):
    user_id = ma.Integer()
    expires_at = ArrowField()
    created_at = ArrowField()

    class Meta:
        model = Token


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
