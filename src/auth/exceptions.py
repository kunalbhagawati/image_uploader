from __future__ import absolute_import, unicode_literals

from src.core.exceptions import Exc


class AuthorizationFailure(Exc):
    """Cannot authorize the user"""
    status_code = 401


class NoAppUserAccessToken(Exc):
    """Critical: App User has no access token!"""
