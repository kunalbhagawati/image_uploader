from __future__ import absolute_import, unicode_literals

from flask_classful import FlaskView
from sqlalchemy.orm.exc import NoResultFound
from webargs import fields
from webargs.flaskparser import parser

from src.auth.models.sqla import User
from src.auth.schemas import TokenSchema
from src.auth.services import login_user
from src.extensions import db

login_args = {'phone_number': fields.String(required=True)}


class LoginView(FlaskView):
    def post(self):
        """
        # Payload
        {
            "phone_number": "+919787348645",
        }
        """
        args = parser.parse(login_args)

        try:
            user = db.session.query(User) \
                .filter(User.phone_number == args['phone_number'].strip()) \
                .one()
        except NoResultFound:
            return {}, 404

        # TODO put proper OTP validation with model

        token = login_user(user.id)
        db.session.commit()
        return {'data': TokenSchema(only=['access_token', 'user_id', 'expires_at']).dump(token)}, 200


def register(app):
    LoginView.register(app, route_base='login')
