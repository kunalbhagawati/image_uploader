from __future__ import absolute_import, unicode_literals

import logging
from typing import Union

import arrow
from sqlalchemy.exc import DataError
from sqlalchemy.orm.exc import NoResultFound

from src.auth.constants import MAX_TOKEN_VALIDITY_HOURS
from src.auth.exceptions import AuthorizationFailure
from src.auth.models.sqla import User, Token
from src.core.db.transaction import flush
from src.core.logging import mark_boundary
from src.core.utils import get_uuid
from src.extensions import db


def _get_token_from_header_string(auth_header_value: str) -> str:
    if not auth_header_value:
        raise AuthorizationFailure('NO_TOKEN_SENT', auth_header_value=auth_header_value)

    token = auth_header_value.strip().lower()

    if not token:
        raise AuthorizationFailure('NO_TOKEN_SENT', auth_header_value=auth_header_value)
    return token


def get_user_from_auth_header(auth_header_value: str) -> Union[User, None]:
    access_token = _get_token_from_header_string(auth_header_value)
    try:
        return db.session.query(User).join(Token)\
            .filter((Token.access_token == access_token) &
                    (Token.expires_at > arrow.utcnow()))\
            .one()
    except NoResultFound:
        return
    except DataError as e:
        raise AuthorizationFailure('TOKEN_FORMAT_WRONG', auth_header_value=auth_header_value) from e


def create_auth_header(token: str):
    return f'{token}'


@flush()
@mark_boundary('LOGIN_USER')
def login_user(user_id: int):
    tokens = db.session.query(Token) \
        .filter(Token.user_id == user_id) \
        .all()

    unexpired_tokens = []
    expired_tokens = []
    for i in tokens:
        if i.expires_at.to('UTC') > arrow.utcnow():
            unexpired_tokens.append(i)
        else:
            expired_tokens.append(i)

    for i in expired_tokens:  # remove the expired ones
        db.session.delete(i)

    if unexpired_tokens:
        token = unexpired_tokens.pop(0)
        if unexpired_tokens:  # still remaining
            logging.exception(f'NON_BLOCKING: FOUND_MORE_THAN_1_UNEXPIRED_TOKENS REMOVING | '
                              f'user_id: {user_id}  token_ids: {[i.id for i in unexpired_tokens]}')
        for i in unexpired_tokens:
            db.session.delete(i)
        return token

    token = Token(user_id=user_id,
                  access_token=get_uuid(),
                  expires_at=arrow.utcnow().shift(hours=MAX_TOKEN_VALIDITY_HOURS),
                  created_by=user_id,
                  modified_by=user_id)
    db.session.add(token)
    return token
