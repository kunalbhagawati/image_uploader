from __future__ import absolute_import, unicode_literals

from flask import g
from flask_cors import CORS
from flask_debugtoolbar import DebugToolbarExtension
from flask_login import LoginManager, user_loaded_from_request
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_pymongo import PyMongo
from flask_sqlalchemy import SQLAlchemy

from .core.db import model, query

db = SQLAlchemy(query_class=query.SoftDeleteHandlingQuery, model_class=model.BaseModel)
migrate = Migrate()
ma = Marshmallow()
toolbar = DebugToolbarExtension()
cors = CORS()
login_manager = LoginManager()
mongo = PyMongo()


@login_manager.request_loader
def load_user_from_request(request):
    from src.auth.services import get_user_from_auth_header
    user = get_user_from_auth_header(request.headers.get('Authorization'))
    g.current_user = user
    return user


@user_loaded_from_request.connect
def user_loaded_from_header(self, user=None):
    g.IS_API_REQUEST = True
