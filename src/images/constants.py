from __future__ import absolute_import, unicode_literals

UPLOAD_LINK_DEFAULT_EXPIRY_SECONDS = 3 * 60 * 60
