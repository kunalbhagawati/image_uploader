from __future__ import absolute_import, unicode_literals

import arrow
from flask import g
from flask_classful import FlaskView, route
from flask_login import login_required
from sentinels import NOTHING
from webargs import fields
from webargs.flaskparser import parser

from .schemas import UploadLinkSchema, ImageSchema
from .services import generate_link, get_link, upload_images, get_image, get_stats

upload_link_new_args = {'expires_at': fields.Function(deserialize=lambda x: arrow.get(x).to('UTC'), missing=NOTHING)}

# TODO change into JSON RFC compliant `{data: [...]}`
upload_images_args = {'data': fields.Nested({'image': fields.Str(required=True),
                                             'metadata': fields.Dict(required=False, missing={})},
                                            required=True,
                                            many=True)}


class UploadLinkView(FlaskView):
    @login_required
    def new(self):
        res = parser.parse(upload_link_new_args)
        link = generate_link(res.get('expires_at'))
        return {'data': UploadLinkSchema().dump(link)}


class ImageView(FlaskView):
    @route('/upload/<string:path>/', methods=['POST'])
    def upload(self, path: str):
        parsed = parser.parse(upload_images_args, validate=lambda args: args)
        link = get_link(path)
        if not link:
            return {}, 404
        images = upload_images(link, parsed['data'])
        return {'data': ImageSchema(exclude=['image']).dump(images, many=True)}

    def get(self, _id: str):
        image = get_image(_id)
        if not image:
            return {}, 404
        return {'data': ImageSchema().dump(image)}


class StatsView(FlaskView):
    @login_required
    def index(self):
        stats = get_stats(user=g.current_user)
        return {'data': stats}


def register(app):
    UploadLinkView.register(app, route_base='upload_links')
    ImageView.register(app, route_base='images')
    StatsView.register(app, route_base='stats')
