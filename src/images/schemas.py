from src.extensions import ma


class IdField(ma.Field):
    """Serializes `_id` and back"""

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attribute', '_id')
        super(IdField, self).__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj, **kwargs):
        return str(obj['_id'])

    def _deserialize(self, value, attr, data, **kwargs):
        return data['id']


class UploadLinkSchema(ma.Schema):
    # TODO send back expires_at as epoch
    id = IdField()

    class Meta:
        additional = ['expires_at']


class ImageSchema(ma.Schema):
    id = IdField()
    metadata = ma.Dict(missing={})

    class Meta:
        additional = ['image', 'name']
