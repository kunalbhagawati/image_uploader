from __future__ import absolute_import, unicode_literals

from src.core.exceptions import Exc


class InvalidLink(Exc):
    """Errors related to upload links"""


class InvalidImage(Exc):
    pass
