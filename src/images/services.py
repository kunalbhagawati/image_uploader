from __future__ import absolute_import, unicode_literals

import base64
from collections import Counter
from io import BytesIO
from typing import Dict, List, Optional

import arrow
from PIL import Image
from arrow import Arrow
from bson import ObjectId
from bson.errors import InvalidId
from flask_login import current_user
from sentinels import NOTHING

from src.auth.models.sqla import User
from src.core.logging import mark_boundary
from src.images.constants import UPLOAD_LINK_DEFAULT_EXPIRY_SECONDS
from src.images.exceptions import InvalidImage
from .exceptions import InvalidLink
from .models.mongo import upload_links, images


@mark_boundary('GENERATE_LINK')
def generate_link(expires_at: Arrow = NOTHING) -> Dict:
    # TODO add buffer for expiry check
    link = upload_links.find_one({'expires_at': {"$gt": arrow.utcnow().datetime}, 'created_by': current_user.id})
    if link:
        return link

    if expires_at is NOTHING:
        expires_at = arrow.utcnow().shift(seconds=UPLOAD_LINK_DEFAULT_EXPIRY_SECONDS)

    if expires_at <= arrow.utcnow():
        raise InvalidLink('LINK_EXPIRED')

    # NOTE exposing ObjectId directly. For added security, should expose a different uuid instead
    link = {'expires_at': expires_at.to('UTC').datetime, 'created_by': current_user.id}

    doc = upload_links.insert_one(link)
    return upload_links.find_one({"_id": doc.inserted_id})


def get_link(path: str) -> Dict:
    """Get the link from mongo"""
    try:
        oid = ObjectId(path)
    except InvalidId as e:
        raise InvalidLink('INVALID_LINK') from e
    return upload_links.find_one({'_id': oid})


def image_from_data(b64string: str) -> Image:
    # not using regex check for simplicity. can otherwise also do https://stackoverflow.com/a/8571649/3297499
    try:
        return Image.open(BytesIO(base64.b64decode(b64string)))
    except IOError as e:
        raise InvalidImage('INVALID_IMAGE_DATA') from e


def upload_images(link: Dict, images_list: List[Dict]) -> List[Dict]:
    # TODO handle binary image data & upload to s3.
    # This base64 will not scale. For now, assuming python & nginx can handle upto 4 MB

    # TODO implement transactions
    # mongodb.com/blog/post/introduction-to-mongodb-transactions-in-python

    if arrow.get(link['expires_at']).to('UTC') <= arrow.utcnow():
        raise InvalidLink("LINK_EXPIRED")

    image_data_set = set()
    for idx, img_dict in enumerate(images_list):
        image_data = img_dict['image']
        if image_data in image_data_set:
            raise InvalidImage('DUPLICATE_IMAGE', f'Duplicate image found in position: {idx}', position=idx)

        img_obj = image_from_data(img_dict['image'])  # do a check to see it is a valid image

        image_data_set.add(img_dict['image'])

        img_dict['format'] = img_obj.format
        img_dict['upload_link'] = link['_id']
        img_dict['created_by'] = link['created_by']

    result = images.insert_many(images_list)

    # Expire link since it is used
    upload_links.update_one({'_id': ObjectId(link['_id'])},
                            {'$set': {"expires_at": arrow.utcnow().datetime}},
                            upsert=False)

    return images.find({'_id': {"$in": result.inserted_ids}})


def get_image(_id: str) -> Dict:
    try:
        oid = ObjectId(_id)
    except InvalidId as e:
        raise InvalidLink('INVALID_LINK') from e
    return images.find_one({'_id': oid})


def get_stats(user: User = None) -> Dict:
    """Get the upload stats. If no user is provided, then get global stats"""
    if user is None:
        raise NotImplemented("GLOBAL_STATS_NOT_IMPLEMENTED")

    stats = {'popularity_charts': {}, 'per_day_frequency': {}}
    image_objs = images.find({'created_by': user.id})
    if not image_objs:
        return stats

    # NOTE Assumes processing in python will be faster than multiple round trips to mongo
    # NOTE unoptimized way, using many iterations for each map. Can be optimized for less loops
    format_count_map = Counter()
    camera_model_map = Counter()
    per_day_frequency = Counter()
    for i in image_objs:
        format_count_map[i['format']] += 1
        camera_model = i['metadata'].get('camera_model')
        if camera_model:
            camera_model_map[camera_model] += 1
        day_start, _ = arrow.get(i['_id'].generation_time).span('day')
        per_day_frequency[day_start.timestamp] += 1  # TODO is in UTC/Mongo offset. Change to localised later.

    def highest(counter: Counter) -> Optional[List[str]]:
        # NOTE inefficient re-looping. Make faster if required
        if not counter:
            return None

        max_val = max(counter.values())
        return [i for i, c in counter.items() if c == max_val]

    stats['popularity_charts'].update({'image_format': highest(format_count_map),
                                       'camera_model': highest(camera_model_map)})
    stats['per_day_frequency'] = per_day_frequency
    return stats
