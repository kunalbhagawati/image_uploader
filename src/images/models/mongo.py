from src.extensions import mongo

upload_links = mongo.db.upload_links
upload_links.create_index("expire_at", expireAfterSeconds=0)

images = mongo.db.images
images.create_index("created_by")
