from __future__ import absolute_import, unicode_literals

import importlib
import logging
from contextlib import suppress
from logging.config import dictConfig

import simplejson
from flask import g, request, Flask, Response
from flask.sessions import SecureCookieSessionInterface
from webargs import fields
from webargs.flaskparser import parser
from werkzeug.exceptions import HTTPException, ImATeapot

from src.core.exceptions import Exc


class CustomSessionInterface(SecureCookieSessionInterface):
    """Prevent creating session from API requests."""

    def save_session(self, *args, **kwargs):
        if g.get('IS_API_REQUEST'):
            return
        return super().save_session(*args, **kwargs)


class JSONResponse(Response):
    """Extend flask.Response with support for list/dict conversion to JSON."""

    def __init__(self, content=None, *args, **kargs):
        if isinstance(content, (list, dict)):
            kargs['mimetype'] = 'application/json'
            content = simplejson.dumps(content), "\n"

        super(Response, self).__init__(content, *args, **kargs)

    @classmethod
    def force_type(cls, response, environ=None):
        """Override with support for list/dict."""
        if isinstance(response, (list, dict)):
            return cls(response)
        else:
            return super(Response, cls).force_type(response, environ)


def create_app():
    """Application factory to create a new app based on the configuration."""
    # Used factory pattern since that is the convention for an app that can (or at least plans to) take
    # multiple configurations.

    # Its better to import the config here since it calls the .env on module load to set the
    # FLASK_ENV and FLASK_DEBUG values (along with the rest of the config) for the flask app which,
    # for whatever unholy reason, is required by the `Flask` app instance even before instantiating.
    #
    # NOTE If you do plan to import this on top, make sure *all* the things importing this module will
    # not get affected by the .env import
    # - Kunal 20-05-2018
    import config

    handle_logging_config(config.config)

    if config.config.ENABLE_HTTP_DEBUG:
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)

    # TODO request/response enveloping, de-enveloping
    app_inst = Flask(__name__)
    app_inst.response_class = JSONResponse
    app_inst.config.from_object(config.config)
    register_extensions(app_inst)
    register_views(app_inst)

    @app_inst.route('/')
    def index():
        return u'👍'

    @app_inst.route('/health_check', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
    def health_check():
        from src.core.exceptions import Exc

        raise_exc_type = parser.parse({'raise_exc_type': fields.Int(missing=0)})['raise_exc_type']
        if raise_exc_type == 0:
            return {'status': "SUCCESS",
                    'request': {'method': request.method}}
        elif raise_exc_type == 1:
            raise Exc(f'ASKED_TO_RAISE', desc='Some desc', a=1, b=2)
        elif raise_exc_type == 2:
            parser.parse({'x': fields.Int(required=True,
                                          error_messages={'required': 'webargs.ValidationError: REQUIRED_FAILED'})})
        elif raise_exc_type == 3:
            raise Exception(f'{Exception.__name__} ASKED_TO_RAISE')
        elif raise_exc_type == 4:
            raise ImATeapot()

    @app_inst.errorhandler(Exc)
    def handle_api_exception(exc: Exc):
        extra = {k: str(v) for k, v in getattr(exc, 'extra', {}).items()}
        desc = getattr(exc, 'desc', None)
        if exc.status_code // 100 == 5:
            logging.exception(exc, extra={'exc__extra': extra, 'exc__desc': desc})
        return {'messages': exc.detail, 'desc': desc, 'extra': extra}, exc.status_code

    # --- Exception handling
    @app_inst.errorhandler(HTTPException)
    def handle_http_exception(exc: HTTPException):
        should_log = False
        msg = getattr(exc, 'data', getattr(exc, 'name', 'UNDEFINED'))
        desc = getattr(exc, 'description', None)
        if not hasattr(exc, 'code'):
            should_log = True
            code = 500
        else:
            code = exc.code
            if code // 100 == 5:
                should_log = True

        if should_log:
            logging.exception(exc.__class__.__name__, extra={'exc__messages': msg, 'exc__desc': desc})
        with suppress(Exception):
            msg.pop('schema', None)  # remove trouble causing un jsonable key
        return {'messages': msg, 'desc': desc, 'extra': {}}, code

    @app_inst.errorhandler(Exception)
    def handle_generic_exception(exc):
        if app_inst.debug:
            raise exc
        else:
            logging.exception(exc)
            return {'messages': str(exc)}, 500

    # ---

    app_inst.session_interface = CustomSessionInterface()

    return app_inst


def handle_logging_config(config_class):
    dictConfig(config_class.LOGGING_CONFIG)


def register_extensions(app_inst):
    from . import extensions

    extensions.cors.init_app(app_inst)
    extensions.db.init_app(app_inst)
    extensions.migrate.init_app(app_inst, extensions.db)
    extensions.ma.init_app(app_inst)
    extensions.toolbar.init_app(app_inst)
    extensions.login_manager.init_app(app_inst)
    extensions.mongo.init_app(app_inst)


def register_views(app_inst):
    load_dirs = ['auth', 'images']
    for d in load_dirs:
        module = importlib.import_module(f'src.{d}.views')
        module.register(app_inst)
