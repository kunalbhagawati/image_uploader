from __future__ import absolute_import, unicode_literals

import uuid


def get_uuid() -> str:
    """Get a random UUID (4)"""
    return uuid.uuid4().hex


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
