from __future__ import absolute_import, unicode_literals

import arrow
from marshmallow import fields


class ArrowField(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        if not value:
            return None
        return value.timestamp

    def _deserialize(self, value, attr, data, **kwargs):
        if not value:
            return None
        return arrow.get(value).to('UTC')
