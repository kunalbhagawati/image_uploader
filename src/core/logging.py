from __future__ import absolute_import, unicode_literals

from contextlib import contextmanager

from src.core.utils import get_uuid


@contextmanager
def mark_boundary(marker: str):
    """Sets a logging boundary with a unique uuid"""

    import logging

    boundary_uuid = get_uuid()

    logging.info(f'< {boundary_uuid} ----- {marker}')
    try:
        yield
    except:
        logging.info(f'!! {marker} ----- EXCEPTION (RE_RAISING)')
        raise
    finally:
        logging.info(f'----- {boundary_uuid} >')
