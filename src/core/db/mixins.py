from __future__ import absolute_import, unicode_literals

import logging

from flask import g
from sqlalchemy import func
from sqlalchemy_utils import ArrowType

from src.extensions import db

logger = logging.getLogger()


class EntryTimestampMixin(object):
    created_at = db.Column(ArrowType, server_default=func.now(), nullable=False)


class ModifiedTimestampMixin(object):
    modified_at = db.Column(ArrowType,
                            server_default=func.now(),
                            server_onupdate=func.now(),
                            nullable=False)


def get_internal_user(context):
    if context.current_parameters.get('modified_by'):
        return context.current_parameters['modified_by']
    logger.warning('This mode of automatically putting the user from the global `g` object is marked for deprecation')
    return g.current_user.id


class TrackedEntryUserMixin(EntryTimestampMixin):
    """
    For setting created_at, and created_by automagically
    """
    created_by = db.Column(db.Integer, default=get_internal_user, nullable=False)


class TrackedModificationsMixin(TrackedEntryUserMixin, ModifiedTimestampMixin):
    modified_by = db.Column(db.Integer,
                            default=get_internal_user,
                            onupdate=get_internal_user,
                            nullable=False)


class SoftDeletesMixin(object):
    """
    Sets a soft deleted column and excludes those rows.
    To get the rows with the results use `_with_deleted=True` in the .query() part.
    """

    __soft_deletes__ = True

    # http://stackoverflow.com/questions/23198801/sqlalchemy-using-aliased-in-query-with-custom-primaryjoin-relationship
    deleted_at = db.Column(ArrowType)
