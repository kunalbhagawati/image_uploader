from __future__ import absolute_import, unicode_literals

from flask_sqlalchemy import Model
from sqlalchemy import event, Column, Integer
from sqlalchemy_utils.models import generic_repr


@generic_repr
class BaseModel(Model):
    """Base that _always_ includes a primary key and disables soft deletes"""

    __abstract__ = True
    __soft_deletes__ = False

    id = Column(Integer, primary_key=True)


@event.listens_for(BaseModel, "instrument_class", propagate=True)
def extra_table_args(mapper, cls):
    """Remove the attr `__extra_table_args` and put those to the actual table defination"""
    if '__extra_table_args__' in cls.__dict__:
        cls.__table__._init_items(*cls.__extra_table_args__)
        del cls.__extra_table_args__
