from __future__ import absolute_import, unicode_literals

import arrow
import pytest

from src.auth.constants import UserRoleEnum
from src.auth.models.sqla import User, Token
from src.core.db.transaction import atomic
from src.core.utils import get_uuid
from src.extensions import db


def test_basic_assert():
    """Just verify pytest is running fine."""
    assert True is True

    with pytest.raises(AssertionError):
        assert True is False


def test_basic_sql():
    db.session.execute('SELECT 1')


def test_basic_session_commit():
    u = User(name='test', phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.commit()


def test_basic_session_double_commit():
    u = User(name='test', type=UserRoleEnum.GENERIC, phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.commit()

    j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
    db.session.add(j)
    db.session.flush()
    db.session.commit()


def test_basic_session_rollback():
    u = User(name='test', type=UserRoleEnum.GENERIC, phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.rollback()


def test_basic_session_nested_commit():
    u = User(name='test', type=UserRoleEnum.GENERIC, phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    with atomic():
        j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
        db.session.add(j)

    rows = db.session.query(Token).all()
    assert len(rows) == 1

    db.session.commit()


def test_basic_session_nested_rollback():
    u = User(name='test', type=UserRoleEnum.GENERIC, phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    with atomic():
        j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
        db.session.add(j)

    rows = db.session.query(Token).all()
    assert len(rows) == 1

    db.session.rollback()


def test_basic_session_nested_failure():
    u = User(name='test', type=UserRoleEnum.GENERIC, phone_number='+91-1112223334', created_by=-1, modified_by=-1)
    db.session.add(u)
    db.session.flush()
    try:
        with atomic():
            j = Token(user=u, access_token=get_uuid(), expires_at=arrow.utcnow())
            db.session.add(j)
            raise RuntimeError
    except:
        pass

    rows = db.session.query(Token).all()
    assert len(rows) == 0

    rows = db.session.query(User).all()
    assert len(rows) == 1

    db.session.commit()


def test_session_is_isolated():
    # TODO set up a way to put pre-filled data in case this test is run first or use pytest.mark hooks to run this last
    rows = db.session.query(Token).all()
    assert len(rows) == 0
