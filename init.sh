#!/usr/bin/env bash

export FLASK_APP=src.app:create_app
export FLASK_ENV=development

flask db upgrade  # run db migrations

flask run -h 0.0.0.0
