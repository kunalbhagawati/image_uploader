"""Common config class for the *entire* app. This includes the flask instance, celery, whatever have you"""
from __future__ import absolute_import, unicode_literals

import logging
import os
from pathlib import Path

from dotenv import load_dotenv
from sqlalchemy.engine.url import URL

basedir = Path(os.path.abspath(os.path.dirname(__file__)))
CONFIG_LOADED = False

ENV_KEY_PREFIX = 'IMAGE_UPLOADER'.upper()

def ek(key_name: str, prefix=ENV_KEY_PREFIX):
    """Transforms the key into PREFIX__KEY"""
    
    return f"{prefix}__{key_name}"


def setup_env(config_name: str = None, reload: bool = False):
    """Sets up the flask env to enable the app to load"""
    global CONFIG_LOADED
    should_load = not CONFIG_LOADED
    if reload:
        should_load = True

    if not should_load:
        return

    # Flask wants the `FLASK_ENV` and `FLASK_DEBUG` (if you want to explicitly control debug mode)
    # to be present before the app instantiates. So we load it upfront here.
    # http://flask.pocoo.org/docs/1.0/config/#environment-and-debug-features
    # - Kunal 20/05/2018
    # BE AWARE, this will *not* overwrite the env if already set e.g Pycharm
    logging.debug(f'LOADING_BASE_ENV_FILE')
    load_dotenv(basedir / '.env', verbose=True)

    if not config_name:
        config_name = os.getenv(ek('CONFIG_NAME'), 'local').strip()

    file_name = f'.env.{config_name}'
    env_file = basedir / file_name
    if not env_file.exists():
        raise ValueError(f'ENV_FILE_DOES_NOT_EXIST | env_file_path: {os.path.abspath(env_file)}')

    logging.info(f'LOADING_EXPLICIT_ENV_FILE | {file_name}')
    load_dotenv(env_file, override=True, verbose=True)  # BUT.. This does overwrite the env

    # > NOTE if for whatever reason the config changes from .env to say, YAML or JSON, or some other scheme, *BE SURE*
    # to load `FLASK_ENV` and `FLASK_DEBUG` (if you want to override that) here.
    # `os.environ['FLASK_ENV'] = whatever`
    # http://flask.pocoo.org/docs/1.0/config/#environment-and-debug-features
    # - Kunal 20/05/2018
    CONFIG_LOADED = True


setup_env()  # NOTE this MUST be called at the top of the file to load the env


class _Config(object):
    """
    Base config class.

    DO NOT PASS THIS CLASS DIRECTLY EVEN THOUGH FLASK SUPPORTS.
    We want to have class properties which are computed at import time, so we will initialize it in the subclasses.
    """
    # NOTE Be aware that the flask env and debug must be set before this config is loaded

    # If we make this class have a metaclass that can instrument all its upper case attributes as properties on class
    # construction, that is, each uppercase attribute gets converted to a property, then we'd have a simple way of
    # remove that env bootup up there ^^^, since the property is just a function that gets the value at runtime,
    # and not at class construction i.e its an instance value, and will not be evaluated at module load.
    # But the problem is what if you don't want to re-compute the value every time? Or if say, the uppercase attr is
    # not a simple attribute (eg its a method or property, etc). Then we'd need to check for that too.
    # The point is the metaclass needs to do a deferred instrumentation of all these values at class construction.
    # So due to lack of time..
    # - Kunal 20/05/2018

    SECRET_KEY = os.environ.get('SECRET_KEY', 'rand0m-seCret-Key')
    APP_DIR = basedir  # Project Folder
    ASSETS_DIR = APP_DIR / 'assets'

    TESTING = bool(int(os.environ[ek('TESTING')]))

    @property
    def SQLALCHEMY_DATABASE_URI(self) -> str:
        return str(URL(os.environ[ek('DB__DRIVER')],
                       username=os.environ[ek('DB__USERNAME')],
                       password=os.environ[ek('DB__PASSWORD')],
                       host=os.environ[ek('DB__HOST')],
                       database=os.environ[ek('DB__NAME')],
                       port=os.environ[ek('DB__PORT')]))

    @property
    def MONGO_URI(self) -> str:

        def m(k: str) -> str:
            return os.environ[ek(f'MONGO__{k.upper()}')]

        return f"mongodb://{m('username')}:{m('password')}@{m('host')}:{m('port')}/{m('name')}?authSource=admin"

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    LOGGING_CONFIG = {
        'version': 1,
        'disable_existing_loggers': False,
        "root": {
            "level": os.getenv(ek('ROOT_LOG_LEVEL'), "INFO").upper(),
            "handlers": ["console"]
        },
        'formatters': {
            'verbose': {
                'format': '[%(process)d %(processName)s %(thread)d %(threadName)s] [%(levelname)s %(asctime)s.%(msecs)03d %(name)s %(module)s:%(lineno)d] > %(message)s',
                'datefmt': '%Y-%m-%dT%H:%M:%S'
            },
            'simple': {
                'format': '[%(levelname)s %(asctime)s.%(msecs)01d %(name)s %(module)s:%(lineno)d] > %(message)s',
                'datefmt': '%Y-%m-%dT%H:%M:%S'
            },
        },
        'filters': {
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': os.getenv(ek('LOGGING_FORMATTER'), 'verbose')
            },
        },
        'loggers': {
            "sqlalchemy.engine": {"level": "INFO"},
            "sqlalchemy.orm": {"level": "WARN"}
        }
    }

    # REDIS = {'HOST': os.environ[ek('REDIS_HOST')],
    #          'DB': os.environ[ek('REDIS_DB')],
    #          'PORT': os.environ[ek('REDIS_PORT')]}

    # REDIS_CELERY = {'HOST': REDIS['HOST'],
    #                 'DB': os.getenv(ek('REDIS_CELERY_DB'), 5),
    #                 'PORT': REDIS['PORT']}

    SENTRY_DSN = os.getenv(ek('SENTRY_DSN'))

    ENABLE_HTTP_DEBUG = os.getenv(ek('ENABLE_HTTP_DEBUG'), 1)

    # AUTHY_API_KEY = os.environ[ek('AUTHY_API_KEY')]

    def __repr__(self):
        vals = '\n\t'.join(f'{key}={getattr(self, key)}' for key in dir(self) if key.isupper())
        return f'{self.__class__.__name__}\n\t{vals}'


config = _Config()
