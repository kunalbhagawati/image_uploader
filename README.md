Simple API to handle image upload. Uses a unique upload link for the uploads.
- Login a user to get an auth token. // Login API  [No auth]
- Creates an Upload link that expires after sometime. // Generate Upload Link API  [Auth token required (header)]
- Put up your images on the link (one time use only, expires after use). // Upload Images API  [No auth]
- Get info about an image // Get image info API  [no auth]
- Check the stats for the images uploaded using the auth token above // Get stats API  [Auth token required (header)]

# External Dependencies
- Docker

# How to run
Create a `.env.local` file if it does not exist:
```shell script
IMAGE_UPLOADER__DB__USERNAME=postgres
IMAGE_UPLOADER__DB__PASSWORD=root

IMAGE_UPLOADER__MONGO__USERNAME=mongo
IMAGE_UPLOADER__MONGO__PASSWORD=mongo
```

Assuming all docker related dependencies are in place, run `docker-compose up`.  
This will spin up the containers, handle the migrations, and expose `localhost:5000` to the host machine to check out the APIs.

Import the postman collection in the repo: `Image Uploader.postman_collection.json`.  
(It will be the "Import" button next to "+ New" button in the top left corner of the app).

*To hit the login API (and thus, the subsequent APIs), you will need to create a user.  
(Using flask console here instead of hitting the DB directly).
```shell script
docker-compose run --rm app bash

# inside the container
flask shell 
```

```python
# inside ipython shell
from src.auth.models.sqla import User
from src.auth.constants import UserRoleEnum                                                                                                                                                        
from src.extensions import db                                                                                                                                                                
u = User(name='admin', type=UserRoleEnum.ADMIN, phone_number='+91-1112223334', created_by=-1, modified_by=-1)                                                                                                          
db.session.add(u)                                                                                                                                                                            
db.session.commit()
```

_NOTE_ If you get problems here and are comfortable hitting the DB directly, you can also do this:
```shell script
docker ps
# get the CONTAINER ID for the postgres container (postgres:11)
docker exec -it "container id" bash

# inside the container
psql -U postgres -d image_uploader
```

```sql
-- inside the postgres shell
INSERT INTO users (created_by, modified_by, name, type, phone_number) VALUES (-1, -1,'admin', 'ADMIN', '+911112223334') RETURNING users.id;
```

# App config
The app takes a default config from the `env` file and then overwrites that with the values in the `.env.local` file.   
https://github.com/theskumar/python-dotenv  

To extend on this, you can pass a env variable 
`IMAGE_UPLOADER__CONFIG_NAME` to the flask app/shell which will then try to load config from the file `.env.<value>` instead of `.env.local`.

for example: local testing where you can set the env value to test and put a file called `.env.test` to override the default 
config. (refer `config.py`)

# API Auth
- Auth tokens must be passed via the `Authorization` header
e.g
```bash
curl -X PATCH \
  http://localhost:5000/<api>/ \
  -H 'Authorization: <token>' \
  -H 'Content-Type: application/json' \
  -d '<payload>'
```

# Tests
Create a `.env.test` file if it does not exist:
```shell script
IMAGE_UPLOADER__TESTING=1

IMAGE_UPLOADER__DB__USERNAME=postgres
IMAGE_UPLOADER__DB__PASSWORD=root
IMAGE_UPLOADER__DB__NAME=image_uploader_test

IMAGE_UPLOADER__MONGO__USERNAME=mongo
IMAGE_UPLOADER__MONGO__PASSWORD=mongo
IMAGE_UPLOADER__MONGO__NAME=image_uploader_test
```

create the database for the test.
```shell script
docker ps
# get the CONTAINER ID for the postgres container (postgres:11)
docker exec -it "container id" bash

# inside the container
psql -U postgres
```

```sql
-- inside the postgres shell
CREATE DATABASE image_uploader_test;
```

`docker-compose run app pytest`

# Tech Choices/Decisions
- flask: lightweight, and we are doing some basic json stuff, no heavy request response conventions to follow.
- postgres: User models need to be stored in a relational way.
- mongo: We need an unstructured data store for the links (as a bonus, supports TTL!) and the images. Mongo is the most widely used that does what we need. 
- redis (lack of): At this scale, mongo + postgres will suffice
