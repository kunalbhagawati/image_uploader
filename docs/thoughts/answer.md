> ## 2) Service scalability and performance
>
> Imagine a thousand of users that constantly upload images to the service from the first task. You have to set up an infrastructure that can handle this traffic. The architecture should contain an asynchronous batch processing, as well as a real-time processing layer.
>
> For instance, batch processing could be used to train and update a predictive model for each of the uploaded images. On the other hand, real-time processing could be used to apply simple data processing (e.g. aggregations over time, data cleaning etc.) or to generate predictions for uploaded images.
>
> How could such an architecture look like? Please provide us with a brief sketch and textual description, and mention *anything* that might seem important to you, for instance:
> - What kind of technologies would you use for the service and its respective infrastructure?

Assuming we talking only about the backend, and not the clients, I'll first give a brief idea of how we can structure it, and talk about what tech helps us achieve that.

<b>
For assumption's sake, let us consider the 3 use-cases given above must be solved.

- train and update a predictive model
- simple data processing (e.g. aggregations over time, data cleaning, etc.)
- generate predictions for uploaded images
</b>
<br>

#### Infrastructure and Architecture
The service has an
- **App** exposed to the external world. This mainly handles API requests/responses, with some commands for admins, etc.
    - The app also exposes a **WebSocket server** to handle real-time updates with the client.
    - Crons go here
- **An async worker system** to handle the background processing. Works on task ids.
- **A data store** to handle the data.

These various services communicate over a **central event backbone**, using a producer/consumer model.

<br>

##### App
Handles requests, exposed to the outside world.
- **Python**: Familiarity, lots of support of scientific / AI / ML stuff
- **Flask**: Light and unopinionated. Scales simpler than Django over time
    - **SQLAlchemy**: unit of work pattern is easier to manage than the active record style.
    - **pymongo**: ODM like `mongoengine` can be put later if more structure is required.

##### Async System
- **RabbitMQ**: More concrete documentation and does not do global pubsub like Redis.
- **Celery**: Most popular in the python world with integrations to Django and Flask
- **Redis**: intermediate store

##### Central event backbone
- RabbitMQ: Complex routing, message state

> - Where and how would you store data?

_I have not worked with AI/ML models before, but have read that they output constantly updating data that have strong relationships._

| use case / resource | data store | cache | why? |
| --- | --- | --- | --- |
| train and update a predictive model | time series database + graph database | not sure | (see assumption above) |
| upload request information | - Simple, fast NoSQL: DynamoDB <br> | no | - no transactional data <br> - acts as log (see below) |
| image | Could Storage: S3 (namespaced by buckets) | NA | file dump |
| image metadata storage | NoSQL: Mongo, etc. | redis if required to display metadata frequently with large number of connections | unstructured in nature. - Does not need transaction guarantees <br> - fast reads for aggregation _(see below)_ |
| image/service stats & aggregations | - Data warehouse (separate service) <br>  - Use NoSQL storage _(see above)_| redis if needs to show same aggregations frequently | - Stats should not be part of the service (leaving aside some that are specific to uploads), and is easier to handle if a separate system already exists for warehousing |

> - On top of what is mentioned above, what would you use the batch/real-time pipeline for?

- **Collaborative clients** for the image and uploads (comments, annotations, etc)
- **Real-time updates to the uploaded images** (kind of like what the learning systems thought-process is).

> - What kind of questions would you need to have answered in order to solve this task more concretely or in order to make better decisions on architecture and technologies?

Here are some questions in order of priority:
- What happens to the uploaded image?
    - Is there post-processing/transformation?
    - Are other services dependant on the ongoing updates / final result?
        - Should this new information be exposed to a data warehouse?
[#single_responsibility, #service_boundary, #hooks, #data_warehouse, #event_driven]

- Can I keep this service small and offload responsibilities to other services (either existing or a new one we have to build)
[#feasibility, #time-resource_constraints]

I can strongly see a need to constantly update a dashboard with
    - stats
    - annotation updates, especially if a lot of users collaborate on one image

- How much real-time data will the external world require? For e.g. Do we need to send back data about stats a lot (kinda like how periscope does)?
[#real-time_requirements]

- How many ongoing connections for one unit of functionality should we support? i.e do we have use cases where multiple connections are trying to change the same values in the data store?
[#resource_level_concurreny]

- Which of these use cases require strong concurrency vs. which of these require strong parallelism?
[#event-driven, #asyncio, #workers]
