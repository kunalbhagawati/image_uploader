FROM python:3.7
SHELL ["/bin/bash", "-c"]

ADD . /code
WORKDIR /code

RUN pip install --upgrade pip
RUN pip install -r requirements/dev.pip
